INSERT INTO Type_De_Train (places, nom_type, class_premiere, vitesse_max)
VALUES (100, 'Express', true, 200),
       (96, 'Local', false, 120),
       (176, 'Rapide', true, 250),
       (160, 'Interurbain', true, 180),
       (300, 'Super Rapide', true, 300),
       (530, 'Regional', false, 160),
       (266, 'Métro', false, 100),
       (410, 'TGV', true, 350),
       (900, 'Transilien', true, 350);

INSERT INTO Gare (nom, ville, adresse, zoneGMT)
VALUES ('Grand Central Terminal', 'New York', '89 E 42nd St, New York, NY 10017, États-Unis', 'UTC -5'),
       ('Gare du Nord', 'Paris', '18 Rue de Dunkerque, 75010 Paris, France', 'UTC +1'),
       ('Shinjuku Station', 'Tokyo', '3-38 Shinjuku, Shinjuku City, Tokyo 160-0022, Japon', 'UTC +9'),
       ('Kings Cross Station', 'Londres', 'Euston Rd, Kings Cross, London N1 9AL, Royaume-Uni', 'UTC +0'),
       ('Central Station', 'Sydney', '1 Eddy Ave, Haymarket NSW 2000, Australie', 'UTC +10'),
       ('Hauptbahnhof', 'Berlin', 'Europaplatz 1, 10557 Berlin, Allemagne', 'UTC +1'),
       ('Union Station', 'Washington, D.C.', '50 Massachusetts Ave NE, Washington, DC 20002, États-Unis', 'UTC -4'),
       ('Stazione Centrale', 'Milan', 'Piazza Duca d''Aosta, 1, 20124 Milano MI, Italie', 'UTC +1'),
       ('Beijing Railway Station', 'Pékin', 'No. 13, Maojiawan Hutong, Dongcheng District, Pékin, Chine', 'UTC +8'),
       ('Estacion Central', 'Santiago', 'Alameda Bernardo O''Higgins 320, Santiago, Chili', 'UTC -4'),
       ('Estacao Central do Brasil', 'Rio de Janeiro', 'Praça Cristiano Ottoni, s/n - Centro, Rio de Janeiro - RJ, Brésil', 'UTC -3'),
       ('Keleti palyaudvar', 'Budapest', 'Budapest, Kerepesi út 2-4, 1087 Hongrie', 'UTC +1'),
       ('Grand Central Station', 'Londres', 'E 42nd St, New York, NY 10017, États-Unis', 'UTC +0'),
       ('Gare Saint Lazare', 'Paris', '13 Rue d''Amsterdam 75008 Paris, France', 'UTC +1'),
       ('Gare de l''Est', 'Paris', 'Place du 11 Novembre 1918, 75010 Paris, France', '1'),
       ('Gare de Lyon', 'Paris', 'Place Louis Armand, 75571 Paris Cedex 12, France', '1'),
       ('Gare Montparnasse', 'Paris', '17 Boulevard de Vaugirard, 75015 Paris, France', '1'),
       ('Gare Saint-Lazare', 'Paris', '13 Rue d''Amsterdam, 75008 Paris, France', '1'),
       ('Gare d''Austerlitz', 'Paris', '85 Quai d''Austerlitz, 75013 Paris, France', '1'),
       ('Gare de Bercy', 'Paris', '48 Bis Boulevard de Bercy, 75012 Paris, France', '1'),
       ('Gare de Strasbourg', 'Strasbourg', '20, place de la Gare, 67000 Strasbourg , France', '1'),
       ('Gare de Marne-la-Vallée - Chessy', 'Marne-la-Vallée', '77777 Marne-la-Vallée, France', '1');

INSERT INTO Train (numero, nom_type)
VALUES (1, 'Express'),
       (2, 'Local'),
       (3, 'Rapide'),
       (4, 'Interurbain'),
       (5, 'Super Rapide'),
       (6, 'Regional'),
       (7, 'Métro'),
       (8, 'TGV'),
       (9,'Transilien');

INSERT INTO Ligne (id,type_train)
VALUES (1,'Express'),
       (2,'Local'),
       (3,'Rapide'),
       (4,'Interurbain'),
       (5,'Super Rapide'),
       (6,'Regional'),
       (7,'Métro'),
       (8,'TGV'),
       (9,'Transilien'),
       (10,'Express'),
       (11,'Rapide'),
       (12,'Interurbain'),
       (13,'Super Rapide');

INSERT INTO Gare_ligne (nom_gare, ville_gare, id_ligne, distance_au_depart)
VALUES ('Gare du Nord', 'Paris', 1, 0),
       ('Gare de Lyon', 'Paris', 1, 3),
       ('Gare de l''Est', 'Paris', 2, 0),
       ('Gare Montparnasse', 'Paris', 3, 5),
       ('Gare Saint-Lazare', 'Paris', 4, 2),
       ('Gare d''Austerlitz', 'Paris', 7, 4),
       ('Gare de Bercy', 'Paris', 9, 6),
       ('Gare de Marne-la-Vallée - Chessy', 'Marne-la-Vallée', 6, 20),
       ('Kings Cross Station', 'Londres', 10, 0),
       ('Central Station', 'Sydney', 8, 10),
       ('Hauptbahnhof', 'Berlin', 11, 0),
       ('Union Station', 'Washington, D.C.', 11, 5),
       ('Stazione Centrale', 'Milan', 12, 0),
       ('Beijing Railway Station', 'Pékin', 12, 15),
       ('Estacion Central', 'Santiago', 13, 0),
       ('Estacao Central do Brasil', 'Rio de Janeiro', 13, 8);

INSERT INTO Billet (id_billet, assure, moyen_paiement )
VALUES (1, true, 'CB'),
       (2, false, 'Cheque'),
       (3, true, 'Espece'),
       (4, true, 'Cheque'),
       (5, false, 'CB'),
       (6, true, 'CB'),
       (7, false, 'CB'),
       (8, true, 'Cheque'),
       (9, true, 'Espece'),
       (10, false, 'Espece');

INSERT INTO Societe_Taxi (nom, nom_Gare, ville_Gare, num_tel)
VALUES
  ('TaxiExpress', 'Grand Central Terminal', 'New York', '+1 555-1234'),
  ('TaxiParis', 'Gare du Nord', 'Paris', '+33073636784'),
  ('TaxiTokyo', 'Shinjuku Station', 'Tokyo', '+1073456784'),
  ('TaxiLondon', 'Kings Cross Station', 'Londres', '+44073456784'),
  ('TaxiSydney', 'Central Station', 'Sydney', '+1 555-7890'),
  ('TaxiBerlin', 'Hauptbahnhof', 'Berlin', '+49073456784'),
  ('TaxiWashington', 'Union Station', 'Washington, D.C.', '+1 555-6789'),
  ('TaxiMilan', 'Stazione Centrale', 'Milan', '+39073456784'),
  ('TaxiBeijing', 'Beijing Railway Station', 'Pékin', '+86073456784'),
  ('TaxiSantiago', 'Estacion Central', 'Santiago', '+56078456784'),
  ('TaxiRio', 'Estacao Central do Brasil', 'Rio de Janeiro', '+55073456784'),
  ('TaxiBudapest', 'Keleti palyaudvar', 'Budapest', '+37073456784'),
  ('TaxiParis2', 'Gare Saint Lazare', 'Paris', '+33072456784'),
  ('TaxiParis3', 'Gare de l''Est', 'Paris', '+33069456784'),
  ('TaxiParis4', 'Gare de Lyon', 'Paris', '+33063456789'),
  ('TaxiParis5', 'Gare Montparnasse', 'Paris', '+33073496789'),
  ('TaxiParis6', 'Gare Saint-Lazare', 'Paris', '+33067456789'),
  ('TaxiParis7', 'Gare d''Austerlitz', 'Paris', '+33073456784'),
  ('TaxiParis8', 'Gare de Bercy', 'Paris', '+33123456788'),
  ('TaxiMarne', 'Gare de Marne-la-Vallée - Chessy', 'Marne-la-Vallée', '+33123456789');

INSERT INTO Hotel (nom, nom_Gare, ville_Gare, places)
VALUES
  ('Hotel New York', 'Grand Central Terminal', 'New York', 150),
  ('Hotel Paris', 'Gare du Nord', 'Paris', 100),
  ('Hotel Tokyo', 'Shinjuku Station', 'Tokyo', 200),
  ('Hotel Londres', 'Kings Cross Station', 'Londres', 120),
  ('Hotel Sydney', 'Central Station', 'Sydney', 250),
  ('Hotel Berlin', 'Hauptbahnhof', 'Berlin', 180),
  ('Hotel Washington', 'Union Station', 'Washington, D.C.', 300),
  ('Hotel Milan', 'Stazione Centrale', 'Milan', 220),
  ('Hotel Pékin', 'Beijing Railway Station', 'Pékin', 180),
  ('Hotel Santiago', 'Estacion Central', 'Santiago', 150),
  ('Hotel Rio de Janeiro', 'Estacao Central do Brasil', 'Rio de Janeiro', 200),
  ('Hotel Budapest', 'Keleti palyaudvar', 'Budapest', 120),
  ('Hotel Paris2', 'Gare Saint Lazare', 'Paris', 100),
  ('Hotel Paris3', 'Gare de l''Est', 'Paris', 150),
  ('Hotel Paris4', 'Gare de Lyon', 'Paris', 200),
  ('Hotel Paris5', 'Gare Montparnasse', 'Paris', 180),
  ('Hotel Paris6', 'Gare Saint-Lazare', 'Paris', 150),
  ('Hotel Paris7', 'Gare d''Austerlitz', 'Paris', 120),
  ('Hotel Paris8', 'Gare de Bercy', 'Paris', 200),
  ('Hotel Marne-la-Vallée', 'Gare de Marne-la-Vallée - Chessy', 'Marne-la-Vallée', 250);

INSERT INTO Transport_public (nom, nom_Gare, ville_Gare, frequence, type)
VALUES ('Metro Paris', 'Gare du Nord', 'Paris', '08:00', 'Metro'),
       ('Tramway Paris', 'Gare du Nord', 'Paris', '10:30', 'Tramway'),
       ('Bus Paris', 'Gare du Nord', 'Paris', '12:45', 'Bus'),
       ('Metro New York', 'Grand Central Terminal', 'New York', '09:15', 'Metro'),
       ('Tramway New York', 'Grand Central Terminal', 'New York', '11:30', 'Tramway'),
       ('Bus New York', 'Grand Central Terminal', 'New York', '13:45', 'Bus'),
       ('Metro Tokyo', 'Shinjuku Station', 'Tokyo', '07:30', 'Metro'),
       ('Tramway Tokyo', 'Shinjuku Station', 'Tokyo', '09:45', 'Tramway'),
       ('Bus Tokyo', 'Shinjuku Station', 'Tokyo', '11:50', 'Bus'),
       ('Metro Londres', 'Kings Cross Station', 'Londres', '08:15', 'Metro'),
       ('Tramway Londres', 'Kings Cross Station', 'Londres', '10:40', 'Tramway'),
       ('Bus Londres', 'Kings Cross Station', 'Londres', '12:55', 'Bus'),
       ('Metro Sydney', 'Central Station', 'Sydney', '07:45', 'Metro'),
       ('Tramway Sydney', 'Central Station', 'Sydney', '10:15', 'Tramway'),
       ('Bus Sydney', 'Central Station', 'Sydney', '12:30', 'Bus'),
       ('Metro Berlin', 'Hauptbahnhof', 'Berlin', '08:30', 'Metro'),
       ('Tramway Berlin', 'Hauptbahnhof', 'Berlin', '10:50', 'Tramway'),
       ('Bus Berlin', 'Hauptbahnhof', 'Berlin', '13:00', 'Bus'),
       ('Metro Washington, D.C.', 'Union Station', 'Washington, D.C.', '08:20', 'Metro'),
       ('Tramway Washington, D.C.', 'Union Station', 'Washington, D.C.', '10:35', 'Tramway'),
       ('Bus Washington, D.C.', 'Union Station', 'Washington, D.C.', '12:40', 'Bus'),
       ('Metro Gare de Lyon 1', 'Gare de Lyon', 'Paris', '08:00', 'Metro'),
       ('Tramway Gare de Lyon 1', 'Gare de Lyon', 'Paris', '10:15', 'Tramway'),
       ('Bus Gare de Lyon 1', 'Gare de Lyon', 'Paris', '12:30', 'Bus'),
       ('Metro Gare Saint-Lazare 1', 'Gare Saint-Lazare', 'Paris', '09:15', 'Metro'),
       ('Tramway Gare Saint-Lazare 1', 'Gare Saint-Lazare', 'Paris', '11:30', 'Tramway'),
       ('Bus Gare Saint-Lazare 1', 'Gare Saint-Lazare', 'Paris', '13:45', 'Bus'),
       ('Metro Gare Montparnasse 1', 'Gare Montparnasse', 'Paris', '08:30', 'Metro'),
       ('Tramway Gare Montparnasse 1', 'Gare Montparnasse', 'Paris', '10:45', 'Tramway'),
       ('Bus Gare Montparnasse 1', 'Gare Montparnasse', 'Paris', '13:00', 'Bus'),
       ('Metro Gare de l''Est 1', 'Gare de l''Est', 'Paris', '09:45', 'Metro'),
       ('Tramway Gare de l''Est 1', 'Gare de l''Est', 'Paris', '12:00', 'Tramway'),
       ('Bus Gare de l''Est 1', 'Gare de l''Est', 'Paris', '14:15', 'Bus'),
       ('Metro Gare de Strasbourg 1', 'Gare de Strasbourg', 'Strasbourg', '08:45', 'Metro'),
       ('Tramway Gare de Strasbourg 1', 'Gare de Strasbourg', 'Strasbourg', '11:00', 'Tramway'),
       ('Bus Gare de Strasbourg 1', 'Gare de Strasbourg', 'Strasbourg', '13:15', 'Bus');


INSERT INTO Voyage (id_voyage, date_validite_debut, date_validite_fin, id_train, type_train)
VALUES(1, 2023-10-15, 2023-10-15, 1,  'Express'),
      (2, 2023-01-05, 2023-01-05, 5,   'Super Rapide'),
      (3, 2023-02-15, 2023-02-15, 6, 'Regional'),
      (4, 2023-05-05, 2023-05-05, 8 , 'TGV'),
      (5, 2023-10-15, 2023-10-15, 9, 'Transilien');

INSERT INTO Arret (ville_gare, nom_gare, id_voyage, arrive, depart)
VALUES ('Paris', 'Gare de Lyon', 1, '8:00', '8:02'),
       ('Paris', 'Gare du Nord', 1, '8:15', '8:17'),
       ('Santiago', 'Estacion Centrale', 2, '16:00', '16:02'),
       ('Marne-la-Vallée', 'Marne-la-Vallée - Chessy', 2, '10:34', '11:02'),
       ('Paris', 'Gare de Lyon', 5, '8:00', '8:02'),
       ('Berlin', 'Hauptbahnhof', 5, '13:56', '14:03');

INSERT INTO Trajet (id_Trajet, numero_place, id_billet, nom_Gare1, ville_Gare1, id_voyage1, nom_Gare2, ville_Gare2, id_voyage2)
VALUES (1, 125, 2, 'Hauptbahnhof', 'Berlin', 5, 'Gare de Lyon','Paris', 5),
       (2, 26, 1, 'Gare du Nord', 'Paris', 1, 'Gare de Lyon','Paris', 1),
       (3, 25, 3, 'Estacion Centrale', 'Santiago', 2, 'Gare de Marne-la-Vallée - Chessy','Marne-la-Vallée', 2);

INSERT INTO Voyageur (nom, prenom, num_telephone, adresse)
VALUES ('Girault', 'Dylan', '07 87 95 36 23', '23 avenue Meunier, Compiègne'),
       ('Kamga', 'Jessica', '07 85 45 78 20', '56 Rue Martin, Compiègne'),
       ('Mercier', 'Matéo', '06 77 23 55 41', '10 rue Leboeuf, Compiègne'),
       ('Delannoy', 'Jules', '06 23 54 65 98', '10 rue Hurtebise, Compiègne');

INSERT INTO Voyageur_special (nom, prenom, num_telephone, numero_carte, rang_carte)
VALUES ('Armani', 'Paul-Antoine', '03 56 48 98 56 56', 1, 'Challenger'),
       ('Mullier', 'Marie', '06 77 36 44 41', 2, 'Gold'),
       ('Marie', 'Martin', '06 45 63 21 41', 3 'Bronze');
