CREATE TABLE Type_De_Train (
  places integer NOT NULL,
  nom_type varchar PRIMARY KEY,
  class_premiere boolean NOT NULL,
  vitesse_max integer NOT NULL
);


CREATE TABLE Gare (
  nom varchar,
  ville varchar,
  adresse varchar NOT NULL,
  zoneGMT varchar NOT NULL, 
  PRIMARY KEY (nom, ville)
);

CREATE TABLE Train (
  numero integer,
  nom_type varchar,
  FOREIGN KEY (nom_type) REFERENCES Type_De_Train(nom_type),
  PRIMARY KEY (numero, nom_type)
);

CREATE TABLE Ligne (
  id INTEGER PRIMARY KEY,
  type_train varchar NOT NULL,
  FOREIGN KEY (type_train) REFERENCES Type_De_Train(nom_type)
);

CREATE TABLE Gare_ligne(
  nom_gare VARCHAR,
  ville_gare VARCHAR ,
  FOREIGN KEY (nom_gare, ville_gare) REFERENCES Gare(nom, ville),
  id_ligne INTEGER,
  FOREIGN KEY (id_ligne) REFERENCES Ligne(id),
  distance_au_depart INTEGER,
  PRIMARY KEY (nom_gare, ville_gare, id_ligne)
);

CREATE TYPE Type_Paiement as ENUM('CB','Espece', 'Cheque');

CREATE TABLE Billet (
  id_billet integer PRIMARY KEY,
  moyen_paiement Type_Paiement NOT NULL,
  assure boolean NOT NULL
);

CREATE TABLE Societe_Taxi(
  nom VARCHAR,
  nom_Gare VARCHAR,
  ville_Gare VARCHAR,
  FOREIGN KEY (nom_gare, ville_gare) REFERENCES Gare(nom, ville),
  num_tel VARCHAR NOT NULL UNIQUE,
  PRIMARY KEY (nom, nom_gare, ville_gare)
);  

CREATE TABLE Hotel (
  nom VARCHAR,
  nom_Gare VARCHAR,
  ville_Gare VARCHAR,
  FOREIGN KEY (nom_gare, ville_gare) REFERENCES Gare(nom, ville),
  places INTEGER NOT NULL,
  PRIMARY KEY (nom, nom_gare, ville_gare)
);

CREATE TYPE Type_transport as ENUM('Metro','Tramway', 'Bus');

CREATE TABLE Transport_public(
  nom VARCHAR,
  nom_Gare VARCHAR,
  ville_Gare VARCHAR,
  FOREIGN KEY (nom_gare, ville_gare) REFERENCES Gare(nom, ville),
  frequence TIME NOT NULL,
  type Type_transport NOT NULL,
  PRIMARY KEY (nom, nom_gare, ville_gare)
);

CREATE TYPE Journee_Semaine AS ENUM (
  'Lundi',
  'Mardi',
  'Mercredi',
  'Jeudi',
  'Vendredi',
  'Samedi',
  'Dimanche',
  'Jour_Ferie'
);

CREATE TABLE Voyage (
  id_voyage integer PRIMARY KEY,
  date_validite_debut date NOT NULL,
  date_validite_fin date NOT NULL,
  id_Train integer NOT NULL,
  type_train varchar NOT NULL,
  FOREIGN KEY (id_Train, type_train) REFERENCES Train(numero, nom_type),
  jour Journee_Semaine NOT NULL
);

CREATE TABLE Arret (
  ville_gare varchar,
  nom_gare varchar,
  FOREIGN KEY (nom_gare, ville_gare) REFERENCES Gare(nom, ville),
  id_voyage integer,
  arrive time,
  depart time,
  check ((arrive IS NOT NULL OR depart IS NOT NULL) AND (arrive IS NULL OR depart IS NULL)),
  PRIMARY KEY (ville_gare, nom_gare, id_voyage)
);

CREATE TABLE Trajet (
  id_Trajet integer PRIMARY KEY,
  numero_place integer NOT NULL,
  id_billet integer NOT NULL,
  FOREIGN KEY (id_billet) REFERENCES Billet(id_billet),
  nom_Gare1 varchar NOT NULL,
  ville_Gare1 varchar NOT NULL,
  id_voyage1 integer NOT NULL,
  FOREIGN KEY (nom_Gare1, ville_Gare1, id_voyage1) REFERENCES Arret(nom_gare, ville_gare, id_voyage),
  nom_Gare2 varchar NOT NULL,
  ville_Gare2 varchar NOT NULL,
  id_voyage2 integer NOT NULL,
  FOREIGN KEY (nom_Gare2, ville_Gare2, id_voyage2) REFERENCES Arret(nom_gare, ville_gare, id_voyage)
);

CREATE TABLE Voyageur (
  nom varchar,
  prenom varchar,
  num_telephone varchar,
  adresse varchar,
  PRIMARY KEY (nom, prenom, num_telephone)
);

CREATE TYPE Carte AS ENUM ('Bronze', 'Silver', 'Gold', 'Platine', 'Diamant', 'Master', 'GrandMaster', 'Challenger');

CREATE TABLE Voyageur_special (
  nom varchar,
  prenom varchar,
  num_telephone varchar,
  FOREIGN KEY (nom, prenom, num_telephone) REFERENCES Voyageur(nom, prenom, num_telephone),
  numero_carte integer UNIQUE,
  rang_carte Carte NOT NULL,
  PRIMARY KEY (nom, prenom, num_telephone)
);
