## Note de clarification

### Gare
- nom : string {key}
- ville : string {key}
- adresse : string
- zone horaire : string

La gare est associé à des Services via une association.

### Service
 - nom: varchar {key}

Chaque Service est associé à une Gare duquel il est proche

### Hotel
 - nombre_places: nombre

Un hotel est un service particulier il hérite donc de la classe Serice et la spécialise
Le nom du service est le nom de l'hotel.

### Societe_Taxi
 - num_tel: varchar

Une société de taxi est aussi un service particulier donc cette classe hérite de Service.
Le nom du service est donc le nom de la société.

### Transport_Public
 - frequence: temps

Un Transport_Public représente un service de transport collectif proche de la gare et hérite donc aussi de Service.
Le nom du service est ici le nom de la ligne de transport collectif

### Bus
Un Bus représente une ligne de bus donc un transport collectif et hérite donc de Transport_Public.

### Métro
Un Métro représente une ligne de bus donc un transport collectif et hérite donc de Transport_Public.

### Tramway
Un Tramway représente une ligne de bus donc un transport collectif et hérite donc de Transport_Public.


### Train
- numero : number {key}

Un train est défini par son type, il est donc associé à un objet type.

### Type_De_Train
- nom : string {key}
- nombre_place : number
- class_premiere : boolean
- vitesse_max : number

### Ligne
Chaque Ligne connait toutes les gares qu'elle parcoure via une relation Ligne_Gare donnant aussi la distance à un des deux terminus.
Chaque Ligne peut avoir un ou plusieurs Voyages programmées
Chaque Ligne possède un type de train spécifique qui les parcourent, on la retrouve via une association à un objet Type_de_Train

### Voyage
- date_validite_debut : date
- date_validite_fin : date

Les voyages sont programmées périodiquement, il est donc nécessaire de définir un temps de validité ou le voyage est fait
On associe une journée où le voyage s'effectue
Un Voyage est un ensemble d'Arrêt précisant l'arrivée et le départ pour chaque Gare
On voyage se fait sur une ligne et donc est associé à un objet Ligne.

### Journee_Semaine "enumerateur" (Permet de définir les jours où se font les trajets)
- Lundi
- Mardi
- Mercredi
- Jeudi
- Vendredi
- Samedi
- Dimanche
- Jour_ferie

### Arrêt (Sert à définir l'heure de départ et d'arrivée pour chaque Gare pour un Voyage)
- depart: heure
- arrivee: heure
Associe à chaque Gare une heure de départ et d'arrêt

### Billet
- assurer: boolean
Il est lié à un voyageur via une association
Il est composé d'un ou plusieurs Trajet, des objets Trajet lui sont donc associés
Rajout d'une clé artificielle

### Trajet
- numero_place : nombre
- date_depart: date
Le Trajet connaît tout ses arrêts (qui est lié aux Gares et le Train) via des associations à des objets Arrêt.
Rajout d'une clé artificielle

### Voyageur
- Nom : string {key}
- Prenom : string {key}
- adresse : string
- num_telephone : string {key}
- moyen_paiement : string

### Voyageur_special
- numero_carte : nombre

Il est lié à un statut.
Il descend de la Table "Voyageur" car il possède les mêmes attributs et qu'un "voyageur_special" est un "Voyageur" avec des attributs supplémentaires.

### Statut "enumerateur"
- Bronze
- Silver
- Gold
- Platine
