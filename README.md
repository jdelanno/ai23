# Projet du Groupe 2 AI23/NF18

## Name
Gestion des trains

## Description
Le client est une société de chemins de fer, qui souhaite mettre en place un système pour gérer ses gares, ses trains, ses lignes de trains et ses billets. Le projet consistera à fournir une base de données et un système de gestion satisfaisant les désires du client.

## Fichier complémentaire

[Lien vers le sujet](https://gitlab.utc.fr/dgirault/ai23/-/blob/main/projet1_Train.pdf)

[Modèle Conceptuel de Données](https://gitlab.utc.fr/dgirault/ai23/-/blob/main/MCD/PlantUml/MCD.svg)
[Modèle Logique de Données](https://gitlab.utc.fr/dgirault/ai23/-/blob/main/MLD/MLD.md)
[Note de Clarification](https://gitlab.utc.fr/dgirault/ai23/-/blob/main/NDC/NDC.md)

## Authors and acknowledgment
- Dylan GIRAULT
- Jessica KAMGA
- Jules DELANNOY
- Mateo MERCIER
