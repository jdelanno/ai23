
# MLD PROJET
## Relation

- Train(#numero: nombre, #type_name => Type_De_Train.nom_type)

- Type_De_Train(places: nombre, #nom_type: texte, class_premiere: boolean, vitesse_max: nombre)

- Voyage(#id_voyage: nombre, date_validite_debut: date, date_validite_fin: date, id_Train => Train.numero, jour: Journee_Semaine)

- Arrêt(#ville_gare => Gare.ville, #nom_gare => Gare.nom, #id_voyage => Voyage.id_voyage, arrive: heure, depart: heure)

- Ligne(#id: number, type_train => Type_De_Train.nom_type)

- Trajet(#id_Trajet: nombre, numero_place: nombre, id_billet => Billet.id_billet, nom_Gare1 => Arrêt.nom_gare, ville_Gare1 => Arrêt.ville_gare, id_voyage1 => Arrêt.id_voyage, nom_Gare2 => Arrêt.nom_gare, ville_Gare2 => Arrêt.ville_gare, id_voyage2 => Arrêt.id_voyage)

- Gare(#nom: texte, #ville: texte, adresse: texte, zoneGMT: texte)

- Gare_ligne(#nom_gare => Gare.nom, #ville_gare => Gare.ville, #id_ligne => Ligne.id, distance_au_depart: nombre)

- Billet(#id_billet: nombre, moyen_paiement: texte, assuré: boolean)

- Societe_Taxi(#nom: texte, #nom_Gare => Gare.nom, #ville_Gare => Gare.ville, num_tel: varchar)

- Hotel(#nom: texte, #nom_Gare => Gare.nom, #ville_Gare => Gare.ville, places: number)

- Transport_public(#nom: texte, #nom_Gare => Gare.nom, #ville_Gare => Gare.ville, frequence: time, type: Type_Transport)

- Voyageur(#nom: texte, #prenom: texte, #num_telephone: texte, adresse: texte)

- Voyageur_special(#nom => Voyageur.nom, #prenom => Voyageur.prenom, #num_telephone => Voyageur.num_telephone, numero_carte: nombre, rang_carte: Carte)


## Contrainte

### Type_De_Train
- "places" est NOT NULL
- "class_premiere" est NOT NULL
- "vitesse_max" est NOT NULL

### Voyage
- "date_validite_debut" est NOT NULL
- "date_validite_fin" est NOT NULL
- "jour" est NOT NULL
- "id_Train" est NOT NULL

### Arrêt
- "ville_Gare" est NOT NULL
- "nom_Gare" est NOT NULL
- "id_voyage" est NOT NULL
- l'attribut "arrive" et "départ" ne peuvent être NULL en même temps

### Ligne
- "type_train" est NOT NULL

### Trajet
- "id_billet" est NOT NULL
- "ville_Gare2" est NOT NULL
- "nom_Gare2" est NOT NULL
- "id_voyage2" est NOT NULL
- "ville_Gare1" est NOT NULL
- "nom_Gare1" est NOT NULL
- "id_voyage1" est NOT NULL
- "numero_place" est NOT NULL
- "(nom_Gare1, ville_Gare1, id_voyage1)" et "(nom_Gare1, ville_Gare1, id_voyage1)" sont différent

### Gare
- "adresse" est NOT NULL
- "zoneGMT" est NOT NULL

### Billet
- "assurer" est NOT NULL
- "moyen_paiement" est NOT NULL

### Societe_Taxi
- "num_tel" est NOT NULL et UNIQUE

### Hotel
- "places" est NOT NULL

### Transport_public
- "frequence" est NOT NULL
- "type" est NOT NULL

### Voyageur

### Voyageur_special
- "rang_carte" est NOT NULL
- "numero_carte" est UNIQUE
